from inventory_api import marshmallow
from inventory_api.models import Product, Category, Transaction

class ProductSchema(marshmallow.ModelSchema):
	class Meta:
		model = Product

class CategorySchema(marshmallow.ModelSchema):
	class Meta:
		model = Category

class TransactionSchema(marshmallow.ModelSchema):
	class Meta:
		model = Transaction