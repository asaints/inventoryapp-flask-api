from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS

app = Flask(__name__)
cors = CORS(app)

app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///site.db"
# app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://root:password@localhost/inventoryapidatabase"

db = SQLAlchemy(app)
marshmallow = Marshmallow(app)

from inventory_api.api.ProductsAPI import products_api
from inventory_api.api.CategoriesAPI import categories_api
from inventory_api.api.TransactionsAPI import transactions_api

app.register_blueprint(products_api)
app.register_blueprint(categories_api)
app.register_blueprint(transactions_api)