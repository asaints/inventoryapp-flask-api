from datetime import datetime
from flask import Blueprint, jsonify, abort, make_response, request

from inventory_api import db
from inventory_api.models import Transaction, Product
from inventory_api.schemas import TransactionSchema

transactions_api = Blueprint('transactions_api', __name__)

transaction_schema_many = TransactionSchema(many=True)
transaction_schema_single = TransactionSchema()

@transactions_api.route('/getProducts/<int:productId>/transactions', methods=['GET'])
def getProductTransactions(productId):
	producttransactions = Product.query.get(productId).transactions
	producttransactions = transaction_schema_many.dump(producttransactions).data

	return make_response(jsonify(producttransactions), 200)

@transactions_api.route('/getProducts/<int:productId>/transactions/add', methods=['POST'])
def addProductTransaction(productId):
	if not request.json:
		abort(400)

	newtransaction = Transaction(transactionAdd=request.json['transactionAdd'], transactionQty=request.json['transactionQty'], baseproduct=productId)

	db.session.add(newtransaction)
	db.session.commit()

	newtransaction = transaction_schema_single.dump(newtransaction).data

	return make_response(jsonify(newtransaction), 201)

@transactions_api.route('/getProducts/<int:productId>/transactions/<int:transactionId>/update', methods=['PUT'])
def editTransaction(productId, transactionId):
	if not request.json:
		abort(400)

	basetransaction = Transaction.query.get(transactionId)


	if basetransaction == None:
		abort(400)

	basetransaction.transactionAdd = bool(request.json['transactionAdd'])
	basetransaction.transactionQty = int(request.json['transactionQty'])

	db.session.commit()

	basetransaction = transaction_schema_single.dump(basetransaction).data

	return make_response(jsonify(basetransaction), 202)

@transactions_api.route('/getProducts/<int:productId>/transactions/<int:transactionId>/delete', methods=['DELETE'])
def deleteTransaction(productId, transactionId):
	if isinstance(transactionId, int) == False:
		abort(400)

	basetransaction = Transaction.query.get(transactionId)
	db.session.delete(basetransaction)
	db.session.commit()

	return make_response(jsonify({"message": "successfuly removed"}), 200)
