from flask import Blueprint, jsonify, abort, make_response, request

from inventory_api import db
from inventory_api.models import Category
from inventory_api.schemas import CategorySchema

categories_api = Blueprint('categories_api', __name__)

category_schema_many = CategorySchema(many=True)
category_schema_single = CategorySchema()

@categories_api.route('/getCategories', methods=['GET'])
def getCategories():
	categories = Category.query.all()
	categories = category_schema_many.dump(categories).data

	return make_response(jsonify(categories), 200)

@categories_api.route('/getCategories/add', methods=['POST'])
def addCategory():
	if not request.json:
		abort(400)

	newcategory = Category(categoryName=request.json['categoryName'])

	db.session.add(newcategory)
	db.session.commit()

	newcategory = category_schema_single.dump(newcategory).data

	return make_response(jsonify(newcategory), 201)

@categories_api.route('/getCategories/<int:categoryId>/edit', methods=['PUT'])
def editCategory(categoryId):
	if not request.json:
		abort(400)
		
	basecategory = Category.query.get(categoryId)

	if basecategory == None:
		abort(400)#abort stops function execution		

	basecategory.categoryName = request.json['categoryName']
	db.session.commit()

	basecategory = category_schema_single.dump(basecategory).data

	return make_response(jsonify(basecategory), 202)

@categories_api.route('/getCategories/<int:categoryId>/delete', methods=['DELETE'])
def deleteCategory(categoryId):
	if isinstance(categoryId, int) == False:
		abort(400)

	basecategory = Category.query.get(categoryId)
	db.session.delete(basecategory)
	db.session.commit()

	return make_response(jsonify({"message": "successfuly removed"}), 200)
