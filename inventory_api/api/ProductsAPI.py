from flask import Blueprint, jsonify, abort, make_response, request

from inventory_api import db
from inventory_api.models import Product
from inventory_api.schemas import ProductSchema

products_api = Blueprint('products_api', __name__)

product_schema_many = ProductSchema(many=True)
product_schema_single = ProductSchema()

@products_api.route('/getProducts', methods=['GET'])
def getProducts():
	products = Product.query.all()
	products = product_schema_many.dump(products).data

	return make_response(jsonify(products), 200)

@products_api.route('/getProducts/<int:productId>', methods=['GET'])
def getProduct(productId):
	product = Product.query.get(productId)
	# product = Product.query.filter_by(productId=productId).first()

	if product == None:
		abort(400)

	product = product_schema_single.dump(product).data

	return make_response(jsonify(product), 200)

@products_api.route('/getProducts/add', methods=['POST'])
def addProduct():
	if not request.json:
		abort(400)

	newproduct = Product(productName=request.json['productName'])
	# newproduct = {
	# 	"productId": productsmasterlist[-1]['productId'] + 1,
	# 	"productName": request.json['productName'],
	# 	"productCategory": request.json.get('productCategory', ""),
	# 	"productStock": 0
	# }

	db.session.add(newproduct)
	db.session.commit()

	newproduct = product_schema_single.dump(newproduct).data

	return make_response(jsonify(newproduct), 201)

@products_api.route('/getProducts/<int:productId>/edit', methods=['PUT'])
def editProduct(productId):
	if not request.json:
		abort(400)

	# baseproduct = [product for product in productsmasterlist if product['productId'] == productId]

	baseproduct = Product.query.get(productId)

	if baseproduct == None:
		abort(400)#abort stops function execution	

	baseproduct.productName = request.json['productName']
	baseproduct.productCategory = request.json.get('productCategory', baseproduct.productCategory)
	db.session.commit()

	baseproduct = product_schema_single.dump(baseproduct).data

	return make_response(jsonify(baseproduct), 202)

@products_api.route('/getProducts/<int:productId>/delete', methods=['DELETE'])
def deleteProduct(productId):
	if isinstance(productId, int) == False:
		abort(400)

	# for index, product in enumerate(productsmasterlist):
	# 	if product['productId'] == productId:
	# 		del productsmasterlist[index]
	# 		print('break')
	# 		break;

	baseproduct = Product.query.get(productId)
	db.session.delete(baseproduct)
	db.session.commit()

	return make_response(jsonify({"message": "successfuly removed"}), 200)
