data = [
	{
		"productId": 1,
		"productName": "Corned Beef",
		"productCategory": "Food",
		"productStock": 5
	},

	{
		"productId": 2,
		"productName": "Instant Coffee",
		"productCategory": "Food",
		"productStock": 10
	}
]