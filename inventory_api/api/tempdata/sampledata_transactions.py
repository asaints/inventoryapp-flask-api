transactions = [
	{
		"transactionId": 1,
		"transactionAdd": True,
		"transactionQty": 5,
		"transactionDate": "Jun 5, 2019 11:26",
		"productId": 1
	},

	{
		"transactionId": 2,
		"transactionAdd": False,
		"transactionQty": 2,
		"transactionDate": "Jun 5, 2019 11:29",
		"productId": 1
	},

	{
		"transactionId": 3,
		"transactionAdd": True,
		"transactionQty": 2,
		"transactionDate": "Jun 5, 2019 11:30",
		"productId": 2
	},

	{
		"transactionId": 4,
		"transactionAdd": False,
		"transactionQty": 2,
		"transactionDate": "Jun 5, 2019 11:31",
		"productId": 2
	},
]