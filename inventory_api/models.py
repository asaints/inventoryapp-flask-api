from inventory_api import db
from datetime import datetime

class Product(db.Model):
	productId = db.Column(db.Integer, primary_key = True)
	productName = db.Column(db.String(20), unique = True, nullable = False)
	productStock = db.Column(db.Integer, nullable = False, default = 0)
	productStock = db.Column(db.Integer, nullable = False, default = 0)
	productCategory = db.Column(db.String(200), unique = True, nullable = True)
	transactions = db.relationship('Transaction', backref="productId", lazy=True)
	# posts = db.relationship('Post', backref="author", lazy=True)

	def __repr__(self):
		return f"Product('{self.productId}', '{self.productName}')"

class Category(db.Model):
	categoryId = db.Column(db.Integer, primary_key = True)
	categoryName = db.Column(db.String(100), unique = True, nullable = False)

	def __repr__(self):
		return f"Category('{self.categoryId}', '{self.categoryName}')"

class Transaction(db.Model):
	transactionId = db.Column(db.Integer, primary_key = True)
	transactionAdd = db.Column(db.Boolean, nullable = False)
	transactionQty = db.Column(db.Integer, nullable = False, default = 0)
	transactionDate = db.Column(db.DateTime, nullable = False, default = datetime.utcnow)
	baseproduct = db.Column(db.Integer, db.ForeignKey('product.productId'), nullable = False)
	
	# posts = db.relationship('Post', backref="author", lazy=True)

	def __repr__(self):
		return f"Transaction('{self.transactionId}', '{self.transactionDate}', '{self.baseproduct}', '{self.transactionAdd}', '{self.transactionQty}')"
